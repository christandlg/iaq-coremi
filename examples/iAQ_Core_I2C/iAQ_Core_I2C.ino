// iAQ_Core_I2C.ino
//
// shows how to use the iAQ-CoreMI library with the sensor connected via I2C. 
//
// Copyright (c) 2019 Gregor Christandl
//
// connect the iAQ-Core to the Arduino like this:
//
// Arduino - iAQ-Core
// 3.3V ---- VCC
// GND ----- GND
// SDA ----- SDA
// SCL ----- SCL
// other pins should be left unconnected.

#include <Arduino.h>
#include <Wire.h>
#include "iAQCoreI2C.h"

//iAQ-Core I2C address is fixed, so only one sensor is allowed per I2C bus. 
iAQCoreI2C iaq;

void setup()
{
	// put your setup code here, to run once:
	Serial.begin(9600);

	//wait for serial connection to open (only necessary on some boards)
	while (!Serial);

#if defined(ESP32)
	Wire.begin(SDA, SCL);
#elif defined(ESP8266)
	Wire.begin(D2, D3);
#else
	Wire.begin();
#endif 

	//iAQ-Core can operate at a maximum of 100kHz clock speed 
	Wire.setClock(100000L);

#if defined(ESP8266)
	//iAQ-Core uses clock stretching, but the default clock stretching limit must be increased for correct operation. 
	//This is only needed on ESP8266.
	Wire.setClockStretchLimit(1000);
#endif /* defined(ESP8266) */

	if (!iaq.begin())
	{
		Serial.println("begin() failed. check the connection to your iAQ-Core.");
		while (1);
	}
}

void loop()
{
	delay(1000);

	//read the sensor to determine if it holds a valid value. 
	//hasValue MUST be called to update sensor values. 
	Serial.print("sensor has valid value: "); Serial.println(iaq.hasValue() ? "true" : "false");

	if (iaq.isRunin())
		Serial.println("sensor is runin state");

	else if (iaq.isBusy())
		Serial.println("sensor is busy");

	else if (iaq.isValid())
	{
		Serial.print("CO2 equivalent: "); Serial.print(iaq.getCO2()); Serial.println("ppm");
		Serial.print("TVOC equivalent: "); Serial.print(iaq.getTVOC()); Serial.println("ppb");
		Serial.print("Sensor Resistance: "); Serial.print(iaq.getResistance()); Serial.println("Ohm");
	}

	else if (iaq.isError())
		Serial.println("sensor is in error state");
}
