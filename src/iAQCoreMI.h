//Yet Another Arduino ams iAQ-Core air quality sensor library 
// Copyright (c) 2018 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/iaq-coremi
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifndef AS3935MI_H_
#define AS3935MI_H_

#include <Arduino.h>

class iAQCoreMI
{
public:
	enum iaq_core_status_t : uint8_t
	{
		iAQ_CORE_STATUS_OK = 0x00,
		iAQ_CORE_STATUS_BUSY = 0x01,
		iAQ_CORE_STATUS_RUNIN = 0x10,
		iAQ_CORE_STATUS_ERROR = 0x80,
	};

    static const uint8_t iAQ_CORE_I2C_ADDRESS = 0x5A;

	iAQCoreMI();
	virtual ~iAQCoreMI();

    //inintializes the sensor. calls beginInterface() of derived classes to initialize interface
    bool begin();

	//reads the sensor data and returns true if the data contain valid measurement results.
	//@return true if the sensor has valid measurement result.
	bool hasValue();

	//blabla for getCO2().
	int16_t getValue();

	//@return CO2 equivalent (in ppm) or -1 if no data has yet been read. 
	int16_t getCO2();

	//@return sensor status
	uint8_t getStatus();

	//@return sensor resistance value (Ohm) or -1 if no data has been read yet.  
	int32_t getResistance();

	//@return TVOC equivalent (in ppm) or -1 if no data has been read yet. 
	int16_t getTVOC();

	//@return true if sensor data is valid
	bool isValid();

	//@return true if the sensor is still in the runin phase (up to 5min), false otherwise. 
	bool isRunin();

	//@return true if sensor is currenty busy, false otherwise. reading values will fail if sensor is busy. 
	bool isBusy();

	//@return true if sensor is in error state. 
	//If the status is ERROR constantly (or very frequently) this
	//indicates that the module is reading non - realistic values, and
	//the sensor element is probably defective. - iAQ-Core datasheet
	bool isError();

private:
    /*
	reads the masked value from the register.
	@param reg register to read.
	@param mask mask of value.
	@return masked value in register. */
	virtual void readData(byte *data) = 0;

    /*
    initializes the interface. must be implemented by derived classes. */
    virtual bool beginInterface() = 0;

	int16_t co2_;			//CO2 equivalent (in ppm) or -1 if no data has been read yet. 

	int8_t status_;			//sensor status as iaq_core_status_t. 
	int32_t resistance_;	//sensor resistance value (Ohm) or -1 if no data has been read yet.  

	int16_t tvoc_;			//TVOC equivalent (in ppm) or -1 if no data has been read yet. 
};

#endif /* AS3935MI_H_ */