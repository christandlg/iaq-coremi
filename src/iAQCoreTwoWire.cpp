//Yet Another Arduino ams iAQ-Core air quality sensor library 
// Copyright (c) 2018 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/iaq-coremi
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#include "iAQCoreTwoWire.h"

iAQCoreTwoWire::iAQCoreTwoWire(TwoWire *wire) : 
wire_(wire)
{
    //nothing to do here...
}

iAQCoreTwoWire::~iAQCoreTwoWire()
{
    //wire_ is NOT deleted as it may be used elsewhere
    wire_ = nullptr;
}

bool iAQCoreTwoWire::beginInterface()
{
    if (wire_)
        return true;

    return false;
}

void iAQCoreTwoWire::readData(byte * data)
{
	if (!wire_)
		return;

	wire_->requestFrom(iAQ_CORE_I2C_ADDRESS, static_cast<uint8_t>(9));

	for (uint8_t i = 0; i < 9; i++)
		data[i] = wire_->read();
}
