//Yet Another Arduino ams iAQ-Core air quality sensor library 
// Copyright (c) 2018 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/iaq-coremi
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifndef iAQCoreTwoWire_H_
#define iAQCoreTwoWire_H_

#include "iAQCoreMI.h"

#include <Wire.h>

class iAQCoreTwoWire: public iAQCoreMI
{
public:
    iAQCoreTwoWire(TwoWire *wire);
    virtual ~iAQCoreTwoWire();

protected:
    TwoWire *wire_;

private:
    virtual bool beginInterface();

	virtual void readData(byte *data);
};

#endif /* iAQCoreTwoWire_H_ */