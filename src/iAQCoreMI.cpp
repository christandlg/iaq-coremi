//Yet Another Arduino ams iAQ-Core air quality sensor library
// Copyright (c) 2018 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/iaq-coremi
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#include "iAQCoreMI.h"


iAQCoreMI::iAQCoreMI() :
	co2_(-1), 
	status_(0x10), 
	resistance_(-1), 
	tvoc_(-1)
{
	//nothing to do here...
}

iAQCoreMI::~iAQCoreMI()
{
	//nothing to do here...
}

bool iAQCoreMI::begin()
{
	if (!beginInterface()) 
		return false;

	return true;
}

bool iAQCoreMI::hasValue()
{
	byte data[9] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	//read data from sensor (all 9 bytes at once)
	readData(data);

	//extract information from data
	co2_ = (data[0] << 8) | data[1];

	status_ = data[2];

	resistance_ = (data[3] << 24) | (data[4] << 16) | (data[5] << 8) | data[6];

	tvoc_ = (data[7] << 8) | data[8];

	return isValid();
}

int16_t iAQCoreMI::getValue()
{
	return getCO2();
}

int16_t iAQCoreMI::getCO2()
{
	return co2_;
}

uint8_t iAQCoreMI::getStatus()
{
	return status_;
}

int32_t iAQCoreMI::getResistance()
{
	return resistance_;
}

int16_t iAQCoreMI::getTVOC()
{
	return tvoc_;
}

bool iAQCoreMI::isValid()
{
	return (status_ == iAQCoreMI::iAQ_CORE_STATUS_OK);
}

bool iAQCoreMI::isRunin()
{
	return (status_ == iAQCoreMI::iAQ_CORE_STATUS_RUNIN);
}

bool iAQCoreMI::isBusy()
{
	return (status_ == iAQCoreMI::iAQ_CORE_STATUS_BUSY);
}

bool iAQCoreMI::isError()
{
	return (status_ == iAQCoreMI::iAQ_CORE_STATUS_ERROR);
}
